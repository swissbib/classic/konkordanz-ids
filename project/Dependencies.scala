/*
 * Copyright (C) 2020  Günter Hipler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
import sbt.*

object Dependencies {
  // https://mvnrepository.com/artifact/org.mongodb/mongodb-driver-sync
  lazy val mongoDbDriver = "org.mongodb" % "mongodb-driver-sync" % "5.1.3"
  lazy val sslContextFactoryKickstart =  "io.github.hakky54" % "sslcontext-kickstart" % "8.3.5"
  lazy val sslContextFactoryKickstartForPem = "io.github.hakky54" % "sslcontext-kickstart-for-pem" % "8.3.6"
}
