/*
 * Copyright (C) 2020  Günter Hipler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package modules

import com.mongodb.client._
import com.mongodb.{ConnectionString, MongoClientSettings, MongoCredential}
import nl.altindag.ssl.SSLFactory
import nl.altindag.ssl.pem.util.PemUtils
import org.bson.Document
import play.api.Logging
import play.api.inject.ApplicationLifecycle

import java.io.FileInputStream
import javax.inject.Inject
import scala.concurrent.Future

class MongoComponentImpl @Inject() (lifecycle: ApplicationLifecycle)
    extends MongoComponent
    with Logging {
  lifecycle.addStopHook(() => {
    Future.successful {
      logger.info("shutting down client concordance service")
    }
  })

  override val concordanceCollection: MongoCollection[Document] =
    createMongoClient

  private def createMongoClient: MongoCollection[Document] = {
    val certificate = new FileInputStream(getEnv("MONGODB_CLIENT_CERTIFICATE"))
    val key = new FileInputStream(getEnv("MONGODB_CLIENT_KEY"))
    val identityMaterial = PemUtils.loadIdentityMaterial(certificate, key)
    val fileInputStream = new FileInputStream(getEnv("MONGODB_CA_CERTIFICATE"))
    val trustMaterial = PemUtils.loadTrustMaterial(fileInputStream)
    val sslFactory = SSLFactory
      .builder()
      .withIdentityMaterial(identityMaterial)
      .withTrustMaterial(trustMaterial)
      .build()

    val settings = MongoClientSettings
      .builder()
      .applicationName("ConcordanceService")
      .applyConnectionString(
        new ConnectionString(getEnv("MONGODB_CONNECTION_STRING"))
      )
      .applyToSslSettings(builder =>
        builder
          .enabled(true)
          .invalidHostNameAllowed(false)
          .context(sslFactory.getSslContext)
      )
      .credential(
        MongoCredential.createCredential(
          getEnv("MONGODB_USERNAME"),
          getEnv("MONGODB_CREDENTIALS_DATABASE"),
          getEnv("MONGODB_PASSWORD").toCharArray
        )
      )
      .build()

    val mongoClient = MongoClients.create(settings)
    val database = mongoClient.getDatabase(getEnv("MONGODB_DATABASE"))
    database.getCollection(getEnv("MONGODB_COLLECTION"))
  }

  private def getEnv(value: String): String = {
    val envValue: Option[String] = sys.env.get(value)
    if (envValue.isDefined) {
      envValue.get
    } else {
      Option(System.getProperties.get(value)) match {
        case Some(value) => value.toString
        case None =>
          throw new Exception(
            "Environment variable " + value + " not available"
          )
      }
    }
  }
}
