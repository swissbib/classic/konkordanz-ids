/*
 * Copyright (C) 2020  Günter Hipler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package controllers

import com.mongodb.BasicDBObject
import com.mongodb.client.FindIterable
import modules.MongoComponentImpl
import org.bson.Document
import play.api._
import play.api.mvc._

import javax.inject._
import scala.util.matching.Regex

@Singleton
class ConcordanceController @Inject() (
    mongoCollection: MongoComponentImpl,
    val controllerComponents: ControllerComponents
) extends BaseController
    with Logging {

  def record(swissbibid: String): Action[AnyContent] = Action {
    implicit request: Request[AnyContent] =>
      {
        val bdbo = new BasicDBObject()
        bdbo.put("_id", swissbibid)
        val doc: FindIterable[Document] =
          mongoCollection.concordanceCollection.find(bdbo)
        Option(doc.first()) match {
          case Some(value) =>
            logger.info(
              s"found target ${value.getString("target")} for id $swissbibid"
            )
            Redirect(value.getString("target"))
          case _ =>
            logger.info(s"no target found for id $swissbibid")
            Redirect("https://swisscovery.slsp.ch")
        }
      }
  }

  // case class IDGroups (network: String, id: String)
  val idsbb: Regex = "(IDSBB)(.*)".r
  def searchCtrlNumber: Action[AnyContent] = Action {
    implicit request: Request[AnyContent] =>
      {
        val redirect =
          if (
            request.queryString.contains("type") &&
            request.queryString("type").size == 1 &&
            request.queryString("type").head == "ctrlnum" &&
            request.queryString.contains("lookfor") &&
            request.queryString("lookfor").size == 1 &&
            idsbb.matches(request.queryString("lookfor").head)
          ) {
            val matchedId: List[Regex.Match] =
              idsbb.findAllMatchIn(request.queryString("lookfor").head).toList
            matchedId match {
              case h :: _ =>
                s"https://swisscovery.slsp.ch/discovery/search?query=lds02,contains,(IDSBB)${h.group(2)}DSV01&tab=41SLSP_DN_CI&search_scope=DN_and_CI&vid=41SLSP_NETWORK:VU1_UNION&offset=0"
              case _ => "https://swisscovery.slsp.ch"
            }
          } else {
            "https://swisscovery.slsp.ch"
          }
        Redirect(redirect)
      }

  }

}
