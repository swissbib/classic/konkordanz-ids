FROM sbtscala/scala-sbt:eclipse-temurin-jammy-21.0.2_13_1.10.1_2.13.14 AS build

ADD . /build
WORKDIR /build
RUN sbt clean
RUN sbt stage

FROM openjdk:21-slim
COPY --from=build /build/target/universal/stage /app/

WORKDIR /app
CMD [ "./bin/swissbib-legacy-id-lookup" ]