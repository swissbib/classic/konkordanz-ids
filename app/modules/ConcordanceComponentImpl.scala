/*
 * Copyright (C) 2020  Günter Hipler
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package modules

import play.api.inject.ApplicationLifecycle

import java.io.{BufferedReader, InputStream, InputStreamReader}
import java.util.zip.GZIPInputStream
import javax.inject.Inject
import scala.collection.mutable
import scala.concurrent.Future

class ConcordanceComponentImpl@Inject()(
                                         lifecycle: ApplicationLifecycle) extends ConcordanceComponent {

  override val concordanceMapping: scala.collection.mutable.Map[String,String] = readMappings

  lifecycle.addStopHook(() => {
    Future.successful(concordanceMapping.clear())
  })

  private def readMappings = {

    class BufferedReaderIterator(reader: BufferedReader) extends Iterator[String] {
      override def hasNext: Boolean = reader.ready
      override def next(): String = reader.readLine()
    }


    val resource: InputStream = getClass.getResourceAsStream("/data/Konkordanz_sbBB_url_swisscovery-NZ.csv.gz")

    val reader = new BufferedReaderIterator(
      new BufferedReader(
        new InputStreamReader(
          new GZIPInputStream(resource)
        )))

    val mapping: mutable.HashMap[String,String] = mutable.HashMap.empty

    while (reader.hasNext) {
      val splittedLine = reader.next().split(";")
     mapping(splittedLine(0)) = splittedLine(1)
    }
    mapping
  }

}
