# Swissbib Legacy System Number Concordance Resolver
A service to resolve legacy system numbers in links to their new system numbers.

Internal Documentation: https://intranet.unibas.ch/display/UBIT/Swissbib-SLSP+Concordance

## Testing
A few example requests that should be resolved correctly.


### Example Search (IDSBB006000115)

Original request:
```http request
GET https://baselbern.swissbib.ch/Search/Results?type=ctrlnum&lookfor=IDSBB006000115
```

Local request:
```http request
GET http://localhost:9000/Search/Results?type=ctrlnum&lookfor=IDSBB006000115
```

Target redirect:
```http request
GET https://swisscovery.slsp.ch/discovery/search?query=lds02,contains,(IDSBB)006000115DSV01&tab=41SLSP_DN_CI&search_scope=DN_and_CI&vid=41SLSP_NETWORK:VU1_UNION&offset=0
```


### Example Record
Original request:
```http request
GET https://baselbern.swissbib.ch/Record/126360510
```

Local request:
```http request
GET http://localhost:9000/Record/126360510
```

Target redirect:
```http request
GET https://swisscovery.slsp.ch/discovery/search?query=lds02,contains,(NEBIS)000031411EBI01&tab=41SLSP_NETWORK&search_scope=DN_and_CI&vid=41SLSP_NETWORK:VU1_UNION
```
