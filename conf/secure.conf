# Set up Play for HTTPS and locked down allowed hosts.
# Nothing in here is required for REST, but it's a good default.
play {
  http {
    cookies.strict = true

    session.secure = true
    session.httpOnly = true

    flash.secure = true
    flash.httpOnly = true

    forwarded.trustedProxies = ["::1", "127.0.0.1"]
  }

  i18n {
    langCookieSecure = true
    langCookieHttpOnly = true
  }

  filters {
    csrf {
      cookie.secure = true
    }


    hosts {

      # A list of valid hosts (e.g. "example.com") or suffixes of valid hosts (e.g. ".example.com")
      # Note that ".example.com" will match example.com and any subdomain of example.com, with or without a trailing dot.
      # "." matches all domains, and "" matches an empty or nonexistent host.
      allowed = ["localhost", ".k8s.unibas.ch", "127.0.0.1", ".k8s-001.unibas.ch"]

      routeModifiers {
        # If non empty, then requests will be checked if the route does not have this modifier. This is how we enable the
        # anyhost modifier, but you may choose to use a different modifier (such as "api") if you plan to check the
        # modifier in your code for other purposes.
        whiteList = ["anyhost"]

        # If non empty, then requests will be checked if the route contains this modifier
        # The black list is used only if the white list is empty
        blackList = []
      }
    }
  }
}